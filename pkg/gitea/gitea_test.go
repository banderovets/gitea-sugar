package gitea_test

import (
	"testing"

	"gitlab.com/banderovets/gitea-sugar/pkg/gitea"
)

func TestBackupCmd(t *testing.T) {
	backupFileName := "/tmp/gitea-backup-test/gitea-2006-01-02-15-04-05.zip"

	cmd := gitea.BackupCmd("/usr/local/bin/gitea", "/data/gitea/conf/app.ini", backupFileName)

	if cmd.Path != "/usr/local/bin/gitea" {
		t.Errorf("expected %s, got %s", "/usr/local/bin/gitea", cmd.Path)
	}
	if stringSlicesEqual(cmd.Args, []string{"dump", "-c", "/data/gitea/conf/app.ini", "-tempdir", "/tmp", "-f", backupFileName}) {
		t.Errorf("expected %v, got %v", []string{"dump", "-c", "/data/gitea/conf/app.ini", "-tempdir", "/tmp", "-f", backupFileName}, cmd.Args)
	}
}

func stringSlicesEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

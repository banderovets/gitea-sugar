package gitea

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

// BackupCmd creates backup exec.Cmd command
func BackupCmd(giteabin string, giteaConfig string, backupFileName string) *exec.Cmd {
	backupCmd := exec.Command(
		giteabin, "dump", "-c", giteaConfig, "-tempdir", os.TempDir(), "-f", backupFileName,
	)

	backupCmd.Stdout = os.Stdout
	backupCmd.Stderr = os.Stderr
	backupCmd.Stdin = os.Stdin
	return backupCmd
}

// Restore creates restore exec.Cmd command
func Restore(backupFile, basePath, dbType, dbHost, dbName, dbUser, dbPasswd string, dbPort int, restoreDb bool, restoreFS bool) error {
	r, err := zip.OpenReader(backupFile)
	if err != nil {
		return err
	}
	defer r.Close()

	sqlFileName, err := tempFileName()
	if err != nil {
		return err
	}
	defer func() {
		_ = os.Remove(sqlFileName)
	}()

	for _, f := range r.File {
		if restoreFS {
			if f.Name == "app.ini" {
				// app.ini
				if err := expandFile(f, path.Join(basePath, "gitea", "conf", "app.ini"), ""); err != nil {
					return err
				}
			}
			// data/*
			if strings.HasPrefix(f.Name, "data") {
				if err := expandPath(f, path.Join(basePath, "gitea"), "data"); err != nil {
					return err
				}
			}
			// repos/*
			if strings.HasPrefix(f.Name, "repos") {
				if err := expandPath(f, path.Join(basePath, "git", "repositories"), "repos"); err != nil {
					return err
				}
			}
			// git/.ssh/*
			if strings.HasPrefix(f.Name, "git/.ssh") {
				if err := expandPath(f, path.Join(basePath, "git", ".ssh"), "git/.ssh"); err != nil {
					return err
				}
			}
		}

		if restoreDb {
			// gitea-db.sql
			if f.Name == "gitea-db.sql" {
				if err := expandFile(f, sqlFileName, ""); err != nil {
					return err
				}

				if err := dbRestore(dbType, dbHost, dbUser, dbName, dbPasswd, dbPort, sqlFileName); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func dbRestore(dbType string, dbHost string, dbUser string, dbName string, dbPasswd string, dbPort int, sqlFilePath string) error {
	if dbType != "postgres" {
		return fmt.Errorf("database type %s not supported yet", dbType)
	}

	if err := pgRestoreShell(dbHost, dbUser, dbName, dbPasswd, dbPort, sqlFilePath); err != nil {
		return fmt.Errorf("error restoring postgres database - %w", err)
	}
	return nil
}

func pgRestoreShell(dbHost string, dbUser string, dbName string, dbPasswd string, dbPort int, sqlFilePath string) error {
	// Set the PGPASSWORD environment variable
	err := os.Setenv("PGPASSWORD", dbPasswd)
	if err != nil {
		return fmt.Errorf("error setting PGPASSWORD environment variable: %w", err)
	}
	defer func() {
		_ = os.Unsetenv("PGPASSWORD")
	}()

	args := []string{
		"-h", dbHost,
		"-p", fmt.Sprint(dbPort),
		"-U", dbUser,
		"-d", dbName,
		"-f", sqlFilePath,
	}
	cmd := exec.Command("psql", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err = cmd.Run(); err != nil {
		return fmt.Errorf("error executing psql command: %w", err)
	}

	return nil
}

func expandPath(zf *zip.File, dest string, trimPath string) error {
	if zf.FileInfo().IsDir() {
		return createDir(zf, dest, trimPath)
	}
	return expandFile(zf, dest, trimPath)
}

func createDir(zf *zip.File, dest string, trimPath string) error {
	outPath := calculatePath(zf, dest, trimPath)
	log.Printf("Creating dir %s", outPath)

	if err := os.MkdirAll(outPath, 0755); err != nil {
		return fmt.Errorf("error creating dir %s - %w", outPath, err)
	}
	return nil
}

func expandFile(zf *zip.File, dest string, trimPath string) error {
	outPath := calculatePath(zf, dest, trimPath)
	log.Printf("Expanding %s to %s", zf.Name, outPath)

	srcFile, err := zf.Open()
	if err != nil {
		return fmt.Errorf("error opening zip file %s - %w", zf.Name, err)
	}

	outFile, err := os.OpenFile(outPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, zf.Mode())
	if err != nil {
		return fmt.Errorf("error opening file %s - %w", outPath, err)
	}
	defer outFile.Close()

	if _, err := io.Copy(outFile, srcFile); err != nil {
		return err
	}

	return nil
}

func calculatePath(zf *zip.File, dest string, trimPath string) string {
	if trimPath != "" {
		return path.Join(dest, strings.TrimPrefix(zf.Name, trimPath))
	}
	return dest
}

func tempFileName() (string, error) {
	dbf, err := os.CreateTemp("", "gitea-*")
	if err != nil {
		return "", fmt.Errorf("error creating temp file - %w", err)
	}
	if err := os.Remove(dbf.Name()); err != nil {
		return "", fmt.Errorf("error removing temp file - %w", err)
	}
	return dbf.Name(), nil
}

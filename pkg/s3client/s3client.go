package s3client

import (
	"context"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type S3Client struct {
	s3Client *s3.Client
}

// NewFromConfig creates a new S3Client from the given aws.Config.
func NewFromConfig(cfg aws.Config) *S3Client {
	return &S3Client{
		s3Client: s3.NewFromConfig(cfg),
	}
}

// Upload reads the blob from the given reader and uploads it to the given bucket and objectKey path.
func (c S3Client) Upload(ctx context.Context, bucket string, objectKey string, r io.Reader) error {
	_, err := c.s3Client.PutObject(ctx, &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(objectKey),
		Body:   r,
	})
	if err != nil {
		return fmt.Errorf("failed to upload object - %w", err)
	}

	return err
}

// Download reads the blob from the given bucket and objectKey path and writes it to the given writer.
func (c S3Client) Download(ctx context.Context, bucket string, objectKey string, w io.WriterAt) error {
	downloader := manager.NewDownloader(c.s3Client)
	_, err := downloader.Download(ctx, w, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(objectKey),
	})

	return err
}

func (c S3Client) Delete(ctx context.Context, bucket string, objectKey string) error {
	_, err := c.s3Client.DeleteObject(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(objectKey),
	})

	return err
}

func (c S3Client) List(ctx context.Context, bucket string, prefix string) ([]string, error) {
	cmdParams := &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
	}
	if prefix != "" {
		cmdParams.Prefix = aws.String(prefix)
	}

	var keys []string
	paginator := s3.NewListObjectsV2Paginator(c.s3Client, cmdParams)
	for paginator.HasMorePages() {
		page, err := paginator.NextPage(ctx)
		if err != nil {
			return nil, fmt.Errorf("error listing objects - %w", err)
		}

		for _, obj := range page.Contents {
			keys = append(keys, *obj.Key)
		}
	}

	return keys, nil
}

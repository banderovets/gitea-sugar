package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"syscall"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-restores3/config"
	"gitlab.com/banderovets/gitea-sugar/pkg/gitea"
	"gitlab.com/banderovets/gitea-sugar/pkg/s3client"
)

const (
	giteaBasePath = "/data"
)

var (
	dbOnly = flag.Bool("db", false, "restore database")
	fsOnly = flag.Bool("fs", false, "restore filesystem")
)

func run() error {
	flag.Parse()

	restoreDb := *dbOnly
	restoreFS := *fsOnly

	if !restoreDb && !restoreFS {
		restoreDb = true
		restoreFS = true
	}

	ctx := context.Background()

	cfg, err := config.Parse()
	if err != nil {
		return fmt.Errorf("error parsing envconfig - %w", err)
	}

	if err := setUidGid(*cfg); err != nil {
		return fmt.Errorf("error setting uid/gid - %w", err)
	}

	objectKey, err := s3keyParam(flag.Args())
	if err != nil {
		return fmt.Errorf("error getting s3 key param - %w", err)
	}

	log.Printf("Downloading %s", objectKey)
	backupFileName, err := download(ctx, *cfg, objectKey)
	if err != nil {
		return fmt.Errorf("error downloading backup file - %w", err)
	}
	defer func() {
		_ = os.Remove(backupFileName)
	}()
	log.Printf("Downloaded %s", backupFileName)

	dbPort, err := cfg.DBPort()
	if err != nil {
		return fmt.Errorf("error getting db port - %w", err)
	}

	if err := gitea.Restore(backupFileName, giteaBasePath, cfg.DB_TYPE, cfg.DBHost(), cfg.DB_NAME, cfg.DB_USER, cfg.DB_PASSWD, dbPort, restoreDb, restoreFS); err != nil {
		return fmt.Errorf("error restoring backup file - %w", err)
	}

	return nil
}

func setUidGid(cfg config.BackupConfig) error {
	if err := syscall.Setgroups([]int{cfg.USER_GID}); err != nil {
		return fmt.Errorf("setgroups error, setting USER_GID=%d %w", cfg.USER_GID, err)
	}
	if err := syscall.Setuid(cfg.USER_UID); err != nil {
		return fmt.Errorf("setuid error, setting USER_UID=%d %w", cfg.USER_UID, err)
	}
	return nil
}

func s3keyParam(args []string) (string, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("s3 key argument is required")
	}

	s3Key := args[0]
	if s3Key == "" {
		return "", fmt.Errorf("s3 key argument is required")
	}
	return s3Key, nil
}

func download(ctx context.Context, cfg config.BackupConfig, objectKey string) (string, error) {
	f, err := os.CreateTemp("", "gitea-backup-*.zip")
	if err != nil {
		return "", fmt.Errorf("error creating temp file - %w", err)
	}
	defer f.Close()

	s3Client := s3client.NewFromConfig(cfg.ToAWS())
	if err := s3Client.Download(ctx, cfg.AWS_BUCKET, objectKey, f); err != nil {
		return "", fmt.Errorf("error downloading backup file - %w", err)
	}

	return f.Name(), nil
}

func main() {
	if err := run(); err != nil {
		panic(err)
	}
}

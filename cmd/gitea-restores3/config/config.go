package config

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/kelseyhightower/envconfig"
)

type BackupConfig struct {
	AWS_ENDPOINT string `required:"true"`
	AWS_REGION   string `default:"us-east-1"`
	AWS_BUCKET   string `required:"true"`
	AWS_KEY      string `required:"true"`
	AWS_SECRET   string `required:"true"`
	AWS_RETRIES  int    `default:"3"`
	USER_UID     int    `required:"true"`
	USER_GID     int    `required:"true"`
	GITEA_BINARY string `default:"/usr/local/bin/gitea"`
	GITEA_CONFIG string `default:"/data/gitea/conf/app.ini"`
	DB_TYPE      string `required:"true"`
	DB_HOST      string `required:"true"`
	DB_NAME      string `required:"true"`
	DB_USER      string `required:"true"`
	DB_PASSWD    string `required:"true"`
}

func (c BackupConfig) DBHost() string {
	if strings.Contains(c.DB_HOST, ":") {
		parts := strings.Split(c.DB_HOST, ":")
		return parts[0]
	}
	return c.DB_HOST
}

func (c BackupConfig) DBPort() (int, error) {
	if strings.Contains(c.DB_HOST, ":") {
		parts := strings.Split(c.DB_HOST, ":")
		if len(parts) != 2 {
			return 0, fmt.Errorf("invalid db host %s", c.DB_HOST)
		}
		port, err := strconv.Atoi(parts[1])
		if err != nil {
			return 0, fmt.Errorf("invalid db host %s - %w", c.DB_HOST, err)
		}
		return port, nil
	}

	if c.DB_TYPE == "postgres" {
		return 5432, nil
	}
	return 0, fmt.Errorf("database type %s not supported yet", c.DB_TYPE)
}

// Retrieve implements the aws.CredentialsProvider interface.
func (c BackupConfig) Retrieve(ctx context.Context) (aws.Credentials, error) {
	return aws.Credentials{
		AccessKeyID:     c.AWS_KEY,
		SecretAccessKey: c.AWS_SECRET,
	}, nil
}

// ToAWS converts the BackupConfig to an aws.Config.
func (c BackupConfig) ToAWS() aws.Config {
	return aws.Config{
		Region:       c.AWS_REGION,
		BaseEndpoint: aws.String(c.AWS_ENDPOINT),
		Credentials:  c,
	}
}

func Parse() (*BackupConfig, error) {
	var cfg BackupConfig
	if err := envconfig.Process("", &cfg); err != nil {
		return nil, fmt.Errorf("error parsing envconfig - %w", err)
	}
	return &cfg, nil
}

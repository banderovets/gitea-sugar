package keeplast

import (
	"time"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-prunes3/pruner/cappedlist"
)

type KeepLastStrategy struct {
	data *cappedlist.CappedList
}

func New(n int) *KeepLastStrategy {
	return &KeepLastStrategy{
		data: cappedlist.New(n),
	}
}

func (l *KeepLastStrategy) Add(t time.Time, s string) {
	l.data.Add(t, s)
}

func (l *KeepLastStrategy) Get() []string {
	return l.data.Get()
}

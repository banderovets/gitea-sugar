package pruner

import (
	"time"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-prunes3/pruner/keeplast"
)

type PrunerConfig struct {
	KeepLast    int
	KeepHourly  int
	KeepDaily   int
	KeepWeekly  int
	KeepMonthly int
	KeepYearly  int
}

type Strategy interface {
	Add(t time.Time, s string)
	Get() []string
}

type Pruner struct {
	strategies []Strategy
}

func New(cfg PrunerConfig) *Pruner {
	var strategies []Strategy
	if cfg.KeepLast > 0 {
		strategies = append(strategies, keeplast.New(cfg.KeepLast))
	}
	return &Pruner{
		strategies: strategies,
	}
}

func (p *Pruner) Add(t time.Time, s string) {
	for _, strategy := range p.strategies {
		strategy.Add(t, s)
	}
}

func (p *Pruner) Get() []string {
	keys := make(map[string]struct{})
	for _, strategy := range p.strategies {
		for _, key := range strategy.Get() {
			keys[key] = struct{}{}
		}
	}
	res := make([]string, 0, len(keys))
	for key := range keys {
		res = append(res, key)
	}
	return res
}

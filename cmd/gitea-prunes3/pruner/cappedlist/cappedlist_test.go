package cappedlist_test

import (
	"reflect"
	"sync"
	"testing"
	"time"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-prunes3/pruner/cappedlist"
)

func TestCappedList(t *testing.T) {
	TestCases := []struct {
		name   string
		cap    int
		params []struct {
			t time.Time
			s string
		}
		expected []string
	}{
		{
			name: "test_add_0",
			cap:  0,
			params: []struct {
				t time.Time
				s string
			}{},
			expected: []string{},
		},
		{
			name: "test_add_1",
			cap:  1,
			params: []struct {
				t time.Time
				s string
			}{
				{
					t: time.Now(),
					s: "test",
				},
			},
			expected: []string{
				"test",
			},
		},
		{
			name: "test_add_3",
			cap:  3,
			params: []struct {
				t time.Time
				s string
			}{
				{
					t: time.Now().Add(-3 * time.Hour),
					s: "test1",
				},
				{
					t: time.Now().Add(-2 * time.Hour),
					s: "test2",
				},
				{
					t: time.Now().Add(-1 * time.Hour),
					s: "test3",
				},
			},
			expected: []string{
				"test1",
				"test2",
				"test3",
			},
		},
		{
			name: "test_add_3_of_7",
			cap:  3,
			params: []struct {
				t time.Time
				s string
			}{
				{
					t: time.Now().Add(-9 * time.Hour),
					s: "test1",
				},
				{
					t: time.Now().Add(-8 * time.Hour),
					s: "test2",
				},
				{
					t: time.Now().Add(-7 * time.Hour),
					s: "test3",
				},
				{
					t: time.Now().Add(-6 * time.Hour),
					s: "test4",
				},
				{
					t: time.Now().Add(-5 * time.Hour),
					s: "test5",
				},
				{
					t: time.Now().Add(-4 * time.Hour),
					s: "test6",
				},
				{
					t: time.Now().Add(-3 * time.Hour),
					s: "test7",
				},
			},
			expected: []string{
				"test5",
				"test6",
				"test7",
			},
		},
		{
			name: "out_of_order_add_4",
			cap:  3,
			params: []struct {
				t time.Time
				s string
			}{
				{
					t: time.Now().Add(-1 * time.Hour),
					s: "test-1",
				},
				{
					t: time.Now().Add(-3 * time.Hour),
					s: "test-3",
				},
				{
					t: time.Now().Add(-4 * time.Hour),
					s: "test-4",
				},
				{
					t: time.Now().Add(-2 * time.Hour),
					s: "test-2",
				},
			},
			expected: []string{
				"test-3",
				"test-2",
				"test-1",
			},
		},
	}

	for _, tc := range TestCases {
		t.Run(tc.name, func(t *testing.T) {
			l := cappedlist.New(len(tc.expected))
			wg := sync.WaitGroup{}
			wg.Add(len(tc.params))
			for _, p := range tc.params {
				go func(t time.Time, s string) {
					defer wg.Done()
					l.Add(t, s)
				}(p.t, p.s)
			}
			wg.Wait()
			if !reflect.DeepEqual(l.Get(), tc.expected) {
				t.Errorf("expected %v, got %v", tc.expected, l.Get())
			}
		})
	}
}

package cappedlist

import (
	"sort"
	"sync"
	"time"
)

type item struct {
	t time.Time
	s string
}

type CappedList struct {
	sync.RWMutex
	cap  int
	data []item
}

func New(cap int) *CappedList {
	return &CappedList{
		cap:  cap,
		data: make([]item, 0, cap),
	}
}

func (l *CappedList) Add(t time.Time, s string) {
	l.Lock()
	defer l.Unlock()

	l.data = append(l.data, item{
		t: t,
		s: s,
	})
	sort.Slice(l.data, func(i, j int) bool {
		return l.data[i].t.Before(l.data[j].t)
	})
	if len(l.data) > l.cap {
		l.data = l.data[1:]
	}
}

func (l *CappedList) Get() []string {
	l.RLock()
	defer l.RUnlock()

	ret := make([]string, len(l.data))
	for i, item := range l.data {
		ret[i] = item.s
	}
	return ret
}

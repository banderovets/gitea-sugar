package main

import (
	"context"
	"fmt"
	"log"
	"path"
	"regexp"
	"time"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-prunes3/config"
	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-prunes3/pruner"
	"gitlab.com/banderovets/gitea-sugar/pkg/s3client"
)

const (
	keepLast    = 5
	keepYearly  = 1
	keepMonthly = 12
	keepWeekly  = 4
	keepDaily   = 7
	keepHourly  = 24
)

func run() error {
	ctx := context.Background()

	cfg, err := config.Parse()
	if err != nil {
		return fmt.Errorf("error parsing envconfig - %w", err)
	}

	if err := prune(ctx, cfg); err != nil {
		return fmt.Errorf("error pruning - %w", err)
	}

	return nil
}

// parse gitea-2006-01-02-15-04-05-UTC.zip to date
func fileNameToDate(fileName string) (time.Time, error) {
	re := regexp.MustCompile(`gitea-(\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}-UTC).zip`)
	match := re.FindStringSubmatch(fileName)
	if len(match) != 2 {
		return time.Time{}, fmt.Errorf("error parsing file name %s", fileName)
	}

	return time.Parse("2006-01-02-15-04-05-UTC", match[1])
}

func prune(ctx context.Context, cfg *config.PruneConfig) error {
	c := s3client.NewFromConfig(cfg.ToAWS())

	keys, err := c.List(ctx, cfg.AWS_BUCKET, "gitea/")
	if err != nil {
		return fmt.Errorf("error listing objects - %w", err)
	}

	pruner := pruner.New(pruner.PrunerConfig{
		KeepLast:    keepLast,
		KeepHourly:  keepHourly,
		KeepDaily:   keepDaily,
		KeepWeekly:  keepWeekly,
		KeepMonthly: keepMonthly,
		KeepYearly:  keepYearly,
	})

	for _, key := range keys {
		date, err := fileNameToDate(path.Base(key))
		if err != nil {
			return fmt.Errorf("error parsing file name %s - %w", key, err)
		}

		pruner.Add(date, key)
	}

	for _, key := range pruner.Get() {
		log.Println("keeping", key)
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		panic(err)
	}
}

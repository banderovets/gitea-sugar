package config

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/kelseyhightower/envconfig"
)

type PruneConfig struct {
	AWS_ENDPOINT string `required:"true"`
	AWS_REGION   string `default:"us-east-1"`
	AWS_BUCKET   string `required:"true"`
	AWS_KEY      string `required:"true"`
	AWS_SECRET   string `required:"true"`
	AWS_RETRIES  int    `default:"3"`
}

// Retrieve implements the aws.CredentialsProvider interface.
func (c PruneConfig) Retrieve(ctx context.Context) (aws.Credentials, error) {
	return aws.Credentials{
		AccessKeyID:     c.AWS_KEY,
		SecretAccessKey: c.AWS_SECRET,
	}, nil
}

// ToAWS converts the BackupConfig to an aws.Config.
func (c PruneConfig) ToAWS() aws.Config {
	return aws.Config{
		Region:       c.AWS_REGION,
		BaseEndpoint: aws.String(c.AWS_ENDPOINT),
		Credentials:  c,
	}
}

func Parse() (*PruneConfig, error) {
	var cfg PruneConfig
	if err := envconfig.Process("", &cfg); err != nil {
		return nil, fmt.Errorf("error parsing envconfig - %w", err)
	}
	return &cfg, nil
}

package main

import (
	"archive/zip"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"syscall"
	"time"

	"gitlab.com/banderovets/gitea-sugar/cmd/gitea-backups3/config"
	"gitlab.com/banderovets/gitea-sugar/pkg/gitea"
	"gitlab.com/banderovets/gitea-sugar/pkg/s3client"
)

const (
	objectKeyFormat = "gitea/2006/01/02/gitea-2006-01-02-15-04-05-UTC.zip"
)

func run() error {
	ctx := context.Background()

	cfg, err := config.Parse()
	if err != nil {
		return fmt.Errorf("error parsing envconfig - %w", err)
	}

	if err := setUidGid(*cfg); err != nil {
		return fmt.Errorf("error setting uid/gid - %w", err)
	}

	backupFileName, err := tempFileName()
	if err != nil {
		return fmt.Errorf("temp file error - %w", err)
	}
	defer func() {
		_ = os.Remove(backupFileName)
	}()

	bcmd := gitea.BackupCmd(cfg.GITEA_BINARY, cfg.GITEA_CONFIG, backupFileName)
	if err := bcmd.Run(); err != nil {
		return fmt.Errorf("error running backup command - %w", err)
	}

	// git/.ssh/authorized_keys is not included in the backup
	if err := addAuthorizedKeys(backupFileName); err != nil {
		return fmt.Errorf("error adding authorized_keys to backup file - %w", err)
	}

	objectKey, err := upload(ctx, *cfg, backupFileName)
	if err != nil {
		return fmt.Errorf("error uploading backup file - %w", err)
	}
	fmt.Println(objectKey)

	return nil
}

func addAuthorizedKeys(backupFileName string) error {
	zipFile, err := zip.OpenReader(backupFileName)
	if err != nil {
		return fmt.Errorf("error opening zip file - %w", err)
	}
	defer zipFile.Close()

	// TODO: figure out /data from app.ini config
	f, err := os.Open("/data/git/.ssh/authorized_keys")
	if err != nil {
		return fmt.Errorf("error opening authorized_keys file - %w", err)
	}
	defer f.Close()

	var authorizedKeyExists bool
	for _, f := range zipFile.File {
		if f.Name == "git/.ssh/authorized_keys" {
			authorizedKeyExists = true
			break
		}
	}

	if !authorizedKeyExists {
		err := addFileToZip(backupFileName, "git/.ssh/authorized_keys", f)
		if err != nil {
			return fmt.Errorf("error adding authorized_keys to zip - %w", err)
		}
	}
	return nil
}

func addFileToZip(zipFilePath, filePath string, r io.Reader) error {
	zipFile, err := os.OpenFile(zipFilePath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return fmt.Errorf("error opening zip file - %w", err)
	}
	defer zipFile.Close()

	archive := zip.NewWriter(zipFile)
	defer archive.Close()

	f, err := archive.Create(filePath)
	if err != nil {
		return fmt.Errorf("error creating authorized_keys in zip - %w", err)
	}

	if _, err = io.Copy(f, r); err != nil {
		return fmt.Errorf("error copying authorized_keys to zip - %w", err)
	}
	return err
}

func tempFileName() (string, error) {
	backupFile, err := os.CreateTemp("", "gitea-backup-*.zip")
	if err != nil {
		return "", fmt.Errorf("error creating temp file - %w", err)
	}
	if err := os.Remove(backupFile.Name()); err != nil {
		return "", fmt.Errorf("error removing temp file - %w", err)
	}

	return backupFile.Name(), nil
}

func setUidGid(cfg config.BackupConfig) error {
	if err := syscall.Setgroups([]int{cfg.USER_GID}); err != nil {
		return fmt.Errorf("setgroups error, setting USER_GID=%d %w", cfg.USER_GID, err)
	}
	if err := syscall.Setuid(cfg.USER_UID); err != nil {
		return fmt.Errorf("setuid error, setting USER_UID=%d %w", cfg.USER_UID, err)
	}
	return nil
}

func upload(ctx context.Context, cfg config.BackupConfig, fileName string) (string, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return "", fmt.Errorf("error opening backup file - %w", err)
	}
	defer f.Close()

	objectKey := time.Now().UTC().Format(objectKeyFormat)

	c := s3client.NewFromConfig(cfg.ToAWS())
	log.Printf("Uploading %s to %s/%s", fileName, cfg.AWS_BUCKET, objectKey)
	if err := c.Upload(ctx, cfg.AWS_BUCKET, objectKey, f); err != nil {
		return "", fmt.Errorf("error uploading backup file - %w", err)
	}
	return objectKey, nil
}

func main() {
	if err := run(); err != nil {
		panic(err)
	}
}

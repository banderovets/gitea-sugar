package config

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/kelseyhightower/envconfig"
)

type BackupConfig struct {
	AWS_ENDPOINT string `required:"true"`
	AWS_REGION   string `default:"us-east-1"`
	AWS_BUCKET   string `required:"true"`
	AWS_KEY      string `required:"true"`
	AWS_SECRET   string `required:"true"`
	AWS_RETRIES  int    `default:"3"`
	USER_UID     int    `required:"true"`
	USER_GID     int    `required:"true"`
	GITEA_BINARY string `default:"/usr/local/bin/gitea"`
	GITEA_CONFIG string `default:"/data/gitea/conf/app.ini"`
}

// Retrieve implements the aws.CredentialsProvider interface.
func (c BackupConfig) Retrieve(ctx context.Context) (aws.Credentials, error) {
	return aws.Credentials{
		AccessKeyID:     c.AWS_KEY,
		SecretAccessKey: c.AWS_SECRET,
	}, nil
}

// ToAWS converts the BackupConfig to an aws.Config.
func (c BackupConfig) ToAWS() aws.Config {
	return aws.Config{
		Region:       c.AWS_REGION,
		BaseEndpoint: aws.String(c.AWS_ENDPOINT),
		Credentials:  c,
	}
}

func Parse() (*BackupConfig, error) {
	var cfg BackupConfig
	if err := envconfig.Process("", &cfg); err != nil {
		return nil, fmt.Errorf("error parsing envconfig - %w", err)
	}
	return &cfg, nil
}

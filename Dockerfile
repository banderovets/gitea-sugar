FROM golang:latest as build

WORKDIR /app

COPY go.mod go.sum /app/
COPY . /app/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /bin/gitea-backups3 cmd/gitea-backups3/main.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /bin/gitea-restores3 cmd/gitea-restores3/main.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /bin/gitea-prunes3 cmd/gitea-prunes3/main.go


FROM gitea/gitea:latest

RUN apk add --no-cache postgresql

COPY --from=build /bin/gitea-backups3 /usr/local/bin/gitea-backups3
COPY --from=build /bin/gitea-restores3 /usr/local/bin/gitea-restores3
COPY --from=build /bin/gitea-prunes3 /usr/local/bin/gitea-prunes3
